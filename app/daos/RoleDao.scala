package daos

import database.Tables._
import database.dbconfig.db
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object RoleDao {

  def get(id: String): Option[RoleRow] = Await.result(db.run(Role.filter(_.id === id).result.headOption), Duration.Inf)

  def add(newRole: RoleRow): String = {
    val UUID = java.util.UUID.randomUUID().toString
    val withUUID = newRole.copy(id = UUID)
    Await.result(db.run(Role += withUUID), Duration.Inf)
    UUID
  }

  def delete(id: String): Int = Await.result(db.run(Role.filter(_.id === id).delete), Duration.Inf)

  def update(id: String, newRow: RoleRow): Option[RoleRow] = {
    get(id) match {
      case Some(_) =>
        val updateRow = Role.filter(_.id === id).update(RoleRow(id, newRow.name, newRow.organizationId))
        Await.result(db.run(updateRow), Duration.Inf)
        get(id)
      case _ => None
    }
  }

  def getPrivileges(roleId: String): Seq[PrivilegeRow] = {
    val query = Role
      .filter(_.id === roleId)
      .join(RolePrivilege).on(_.id === _.rolId)
      .join(Privilege).on(_._2.privilegeId === _.id)
      .map(_._2)
    Await.result(db.run(query.result), Duration.Inf)
  }

  def addPrivilegeToRole(roleId: String, privilegeId: String): Int = {
    Await.result(db.run(RolePrivilege += RolePrivilegeRow(roleId, privilegeId)), Duration.Inf)
  }

  def removePrivilege(roleId: String, privilegeId: String): Int = {
    Await.result(db.run(RolePrivilege.filter(_.rolId === roleId).filter(_.privilegeId === privilegeId).delete), Duration.Inf)
  }


}
