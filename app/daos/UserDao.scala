package daos

import database.Tables._
import database.dbconfig.db
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object UserDao {

  def get(id: String): Option[UserRow] = Await.result(db.run(User.filter(_.id === id).result.headOption), Duration.Inf)

  def getByEmail(email: String): Option[UserRow] = Await.result(db.run(User.filter(_.email === email).result.headOption), Duration.Inf)

  def exist(email: String): Boolean = getByEmail(email).isDefined

  def getRolesForOrganization(userId: String, orgId: String): Seq[RoleRow] = {
    val query = User
      .filter(_.id === userId)
      .join(UserOrganizationRole).on(_.id === _.userId)
      .filter(_._2.organizationId === orgId)
      .map(_._2.roleId)
      .join(Role).on(_ === _.id)
      .map(_._2)
      .result
    Await.result(db.run(query), Duration.Inf)
  }

  def add(newUser: UserRow): String = {
    val UUID = java.util.UUID.randomUUID().toString
    val withUUID = newUser.copy(id = UUID)
    Await.result(db.run(User += withUUID), Duration.Inf)
    UUID
  }

  def delete(id: String): Int = Await.result(db.run(User.filter(_.id === id).delete), Duration.Inf)

  def removeRoleForUserOrganization(userId: String, orgId: String, roleId: String): Int = {
    val query = UserOrganizationRole
      .filter(_.userId === userId)
      .filter(_.organizationId === orgId)
      .filter(_.roleId === roleId)
      .delete
    Await.result(db.run(query), Duration.Inf)
  }

  def addUserRolesToOrganization(userId: String, orgId: String, roleToAdd: String): Int = Await.result(db.run(UserOrganizationRole += UserOrganizationRoleRow(userId, orgId, roleToAdd)), Duration.Inf)

  def update(id: String, editedUser: UserRow): Option[UserRow] = {
    get(id) match {
      case Some(_) =>
        val updateUser = User.filter(_.id === id).update(UserRow(id, editedUser.email, editedUser.firstName, editedUser.lastName, editedUser.bornDate, editedUser.gender, editedUser.password, editedUser.active))
        Await.result(db.run(updateUser), Duration.Inf)
        get(id)
      case _ => None
    }
  }
}
