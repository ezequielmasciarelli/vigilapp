package daos

import database.Tables._
import database.dbconfig.db
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object PrivilegeDao {

  def get(id: String): Option[PrivilegeRow] = Await.result(db.run(Privilege.filter(_.id === id).result.headOption), Duration.Inf)

  def add(newPrivilege: PrivilegeRow): String = {
    val UUID = java.util.UUID.randomUUID().toString
    val withUUID = newPrivilege.copy(id = UUID)
    Await.result(db.run(Privilege += withUUID), Duration.Inf)
    UUID
  }

  def delete(id: String): Int = Await.result(db.run(Privilege.filter(_.id === id).delete), Duration.Inf)

  def update(id: String, newRow: PrivilegeRow): Option[PrivilegeRow] = {
    get(id) match {
      case Some(_) =>
        val updateRow = Privilege.filter(_.id === id).update(PrivilegeRow(id, newRow.`type`, newRow.description))
        Await.result(db.run(updateRow), Duration.Inf)
        get(id)
      case _ => None
    }
  }
}
