package daos

import database.Tables._
import database.dbconfig.db
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

object OrganizationDao {
  def getRoles(id: String) : Seq[RoleRow] = {
    val query = Role
      .filter(_.organizationId === id)
      .join(Organization).on(_.organizationId === _.id)
      .map(_._1)
      .result
    Await.result(db.run(query), Duration.Inf)
  }


  def get(id: String): Option[OrganizationRow] = Await.result(db.run(Organization.filter(_.id === id).result.headOption), Duration.Inf)

  def getOrganizationOfUser(id: String): Seq[OrganizationRow] = {
    val query = UserOrganization
      .filter(_.userId === id)
      .join(Organization).on(_.organizationId === _.id)
      .map(_._2)
      .result
    Await.result(db.run(query), Duration.Inf)
  }

  def getUsersOfOrganization(id: String): Seq[UserRow] = {
    val query = Organization
      .join(UserOrganization).on(_.id === _.organizationId)
      .join(User).on(_._2.userId === _.id)
      .map(_._2)
      .result
    Await.result(db.run(query), Duration.Inf)
  }

  def add(newOrganization: OrganizationRow): String = {
    val UUID = java.util.UUID.randomUUID().toString
    val withUUID = newOrganization.copy(id = UUID)
    Await.result(db.run(Organization += withUUID), Duration.Inf)
    UUID
  }

  def addUserToOrganization(orgId: String, userId: String): Int = Await.result(db.run(UserOrganization += UserOrganizationRow(orgId, userId)), Duration.Inf)

  def delete(id: String): Int = {
      Await.result(db.run(UserOrganization.filter(_.organizationId === id).delete),Duration.Inf)
      Await.result(db.run(UserOrganizationRole.filter(_.organizationId === id).delete),Duration.Inf)
      Await.result(db.run(Role.filter(_.organizationId === id).delete),Duration.Inf)
      Await.result(db.run(Organization.filter(_.id === id).delete), Duration.Inf)
  }


  def deleteUserFromOrganization(orgId: String, userId: String): Int = {
    val query = UserOrganization.filter(_.organizationId === orgId).filter(_.userId === userId).delete
    Await.result(db.run(query), Duration.Inf)
  }

  def update(id: String, newRow: OrganizationRow): Option[OrganizationRow] = {
    get(id) match {
      case Some(_) =>
        val updateRow = Organization.filter(_.id === id).update(OrganizationRow(id, newRow.name, newRow.fiscalAddress))
        Await.result(db.run(updateRow), Duration.Inf)
        get(id)
      case _ => None
    }
  }
}
