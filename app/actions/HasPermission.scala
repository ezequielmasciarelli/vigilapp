package actions

import javax.inject.{Inject, Singleton}
import play.api.mvc.{ActionFilter, BodyParsers, Result}
import play.api.mvc.Results.Forbidden

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HasPermission @Inject()(val parser: BodyParsers.Default)(implicit val executionContext: ExecutionContext)
  extends ActionFilter[RequestWithContext] {

    def filter[A](input: RequestWithContext[A]): Future[Option[Result]] = Future.successful {
      if (input == null)
        Some(Forbidden)
      else
        None
    }
}
