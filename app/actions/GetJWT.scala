package actions

import database.Tables.UserRow
import javax.inject.{Inject, Singleton}
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim}
import play.api.libs.json.Json
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Success, Try}

@Singleton
class GetJWT @Inject()(val parser: BodyParsers.Default)(implicit val executionContext: ExecutionContext)
  extends ActionBuilder[RequestWithContext, AnyContent]
    with ActionTransformer[Request, RequestWithContext] {

  def transform[A](request: Request[A]): Future[RequestWithContext[A]] = Future.successful {
    val result = request.headers.toSimpleMap.get("Authorization")
      .filter(Jwt.isValid(_, "secretKey", Seq(JwtAlgorithm.HS256)))
      .map(Jwt.decode(_, "secretKey", Seq(JwtAlgorithm.HS256)))
      .flatMap(tryToOption)
      .map(_.content)
      .map(Json.parse)
      .map(_.as[UserRow])
      .map(RequestWithContext(_, request))
      .orNull
    result
  }

  def tryToOption: Try[JwtClaim] => Option[JwtClaim] = {
    case Success(value) => Some(value)
    case _ => None
  }

}

case class RequestWithContext[A](user: UserRow, request: Request[A]) extends WrappedRequest[A](request: Request[A])



