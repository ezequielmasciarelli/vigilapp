package controllers

import actions.{GetJWT, HasPermission}
import daos.{OrganizationDao, RoleDao, UserDao}
import database.Tables._
import javax.inject._
import play.api.libs.json.{Json, Reads}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class OrganizationController @Inject()(cc: ControllerComponents,
                                       getJWT: GetJWT,
                                       hasPermission: HasPermission
                                      ) extends AbstractController(cc) {

  private val isLogged = getJWT andThen hasPermission

  def getOrganizationsForLoggedUser : Action[AnyContent] = isLogged { request =>
    val userId = request.user.id
    val organizations = OrganizationDao.getOrganizationOfUser(userId)
    Ok(Json.toJson(organizations))
  }

  def get(id: String): Action[AnyContent] = isLogged {
    OrganizationDao.get(id) match {
      case Some(user) => Ok(Json.toJson(user))
      case _ => NotFound
    }
  }

  def getRoles(id: String): Action[AnyContent] = isLogged {
    OrganizationDao.get(id)
      .map(_ => OrganizationDao.getRoles(id))
      .map(roles => Ok(Json.toJson(roles)))
      .getOrElse(NotFound)
  }

  def getUsers(id: String): Action[AnyContent] = isLogged {
    OrganizationDao.get(id) match {
      case Some(_) => Ok(Json.toJson(OrganizationDao.getUsersOfOrganization(id)))
      case _ => NotFound
    }
  }

  def add: Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(organizationJson) =>
        val userId = request.user.id
        val organizationId = OrganizationDao.add(organizationJson.as[OrganizationRow])
        val newRole = RoleRow(name = "Admin", organizationId = organizationId)
        val roleId = RoleDao.add(newRole)
        UserDao.addUserRolesToOrganization(userId,organizationId,roleId) match {
          case 1 => OrganizationDao.addUserToOrganization(organizationId,request.user.id) match {
            case 1 => Ok(Json.toJson(OrganizationDao.get(organizationId)))
            case _ => InternalServerError
          }
          case _ => InternalServerError
        }
      case _ => BadRequest
    }
  }

  case class idBody(id: String)

  implicit val privilegeRead: Reads[idBody] = Json.reads[idBody]

  def addUserToOrganization(orgId: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(userJson) =>
        OrganizationDao.addUserToOrganization(orgId, userJson.as[idBody].id) match {
          case 1 => Ok(Json.toJson(OrganizationDao.get(orgId)))
          case _ => BadRequest
        }
      case _ => BadRequest
    }
  }


  def delete(id: String): Action[AnyContent] = isLogged {
    OrganizationDao.delete(id) match {
      case 1 => Ok
      case _ => NotFound
    }
  }

  def deleteUserFromOrganization(orgId: String): Action[AnyContent] = isLogged { implicit request =>
    OrganizationDao.get(orgId) match {
      case Some(_) =>
        request.body.asJson match {
          case Some(json) => OrganizationDao.deleteUserFromOrganization(orgId, json.as[idBody].id) match {
            case 1 => Ok(Json.toJson(OrganizationDao.get(orgId)))
            case _ => BadRequest
          }
          case _ => BadRequest
        }
      case _ => BadRequest
    }
  }

  def update(id: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(organizationJson) =>
        OrganizationDao.update(id, organizationJson.as[OrganizationRow]) match {
          case Some(organization) => Ok(Json.toJson(organization))
          case None => NotFound
        }
      case None => BadRequest
    }
  }

}
