package controllers

import Mail.{Mail, send}
import actions.{GetJWT, HasPermission}
import daos.UserDao
import javax.inject._
import play.api.libs.json.{Json, Reads}
import play.api.mvc._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class MailController @Inject()(cc: ControllerComponents,
                               getJWT: GetJWT,
                               hasPermission: HasPermission
                              ) extends AbstractController(cc) {

  case class RecoveryMailRequest(email: String)

  implicit val privilegeRead: Reads[RecoveryMailRequest] = Json.reads[RecoveryMailRequest]

  def recoveryMail: Action[AnyContent] = Action { request =>
    request.body.asJson match {
      case Some(json) =>
        val email = json.as[RecoveryMailRequest].email
        UserDao.getByEmail(email) match {
          case Some(_) =>
            Future {
              send a Mail(
            from = ("vigilapp@codingmonkey.com.ar", "[Vigilapp] Recupera tu Cuenta"),
            to = Seq(email),
            subject = "Recupera tu cuenta",
            message = "Proximamente te enviaremos el link para recuperar tu cuenta")
            }
            Ok
          case _ => Ok
        }
      case _ =>
        Ok
    }
  }
}