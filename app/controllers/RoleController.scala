package controllers

import actions.{GetJWT, HasPermission}
import daos.RoleDao
import database.Tables._
import javax.inject._
import play.api.libs.json.{Json, Reads}
import play.api.mvc._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class RoleController  @Inject()(cc: ControllerComponents,
                                getJWT: GetJWT,
                                hasPermission: HasPermission
                               ) extends AbstractController(cc) {

  private val isLogged = getJWT andThen hasPermission

  def get(id: String): Action[AnyContent] = isLogged {
    RoleDao.get(id) match {
      case Some(result) =>
        val response = Json.obj(
          "role" -> Json.toJson(result),
          "privileges" -> Json.toJson(RoleDao.getPrivileges(id))
        )
        Ok(response)
      case _ => NotFound
    }
  }

  def getPrivilegesOfRole(id: String): Action[AnyContent] = isLogged {
    RoleDao.get(id) match {
      case Some(_) => Ok(Json.toJson(RoleDao.getPrivileges(id)))
      case _ => NotFound
    }
  }

  def removePrivilegeFromRole(id: String, privilegeId: String): Action[AnyContent] = isLogged {
    RoleDao.removePrivilege(id, privilegeId)
    match {
      case 1 => Ok
      case _ => NotFound
    }
  }

  case class AddPrivilegeToRoleBody(id: String)

  implicit val privilegeRead: Reads[AddPrivilegeToRoleBody] = Json.reads[AddPrivilegeToRoleBody]

  def addPrivilegeToRole(id: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        RoleDao.addPrivilegeToRole(id, json.as[AddPrivilegeToRoleBody].id)
        val response = Json.obj(
          "role" -> Json.toJson(RoleDao.get(id)),
          "privileges" -> Json.toJson(RoleDao.getPrivileges(id))
        )
        Ok(response)
      case _ => BadRequest
    }
  }

  def add: Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        val id = RoleDao.add(json.as[RoleRow])
        Ok(Json.toJson(RoleDao.get(id)))
      case _ => BadRequest
    }
  }

  def delete(id: String): Action[AnyContent] = isLogged {
    RoleDao.delete(id) match {
      case 1 => Ok
      case _ => NotFound
    }
  }

  def update(id: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        RoleDao.update(id, json.as[RoleRow]) match {
          case Some(result) => Ok(Json.toJson(result))
          case None => NotFound
        }
      case None => BadRequest
    }
  }
}
