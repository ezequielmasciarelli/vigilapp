package controllers

import actions.{GetJWT, HasPermission}
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               getJWT: GetJWT,
                               hasPermission: HasPermission
                              ) extends AbstractController(cc) {

  private val isLogged = getJWT andThen hasPermission
  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */

  def api: Action[AnyContent] = Action {
    val json = Json.obj(
      "version" -> "0.0.1"
    )
    Ok(json)
  }

  def rebuild: Action[AnyContent] = isLogged {
    val profile = "slick.jdbc.MySQLProfile"
    val jdbcDriver = "com.mysql.cj.jdbc.Driver"
    val url = "jdbc:mysql://localhost:3306/vigilapp"
    val outputFolder = "app"
    val pkg = "database"
    val user = "root"
    val password = "Maurohernandez123$"
    slick.codegen.SourceCodeGenerator.main(
      Array(profile, jdbcDriver, url, outputFolder, pkg, user, password)
    )
    Ok
  }

}
