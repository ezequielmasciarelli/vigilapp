package controllers

import actions.{GetJWT, HasPermission}
import daos.PrivilegeDao
import database.Tables._
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class PrivilegeController @Inject()(cc: ControllerComponents,
                                    getJWT: GetJWT,
                                    hasPermission: HasPermission
                                   ) extends AbstractController(cc) {

  private val isLogged = getJWT andThen hasPermission

  def get(id: String): Action[AnyContent] = Action {
    PrivilegeDao.get(id) match {
      case Some(result) => Ok(Json.toJson(result))
      case _ => NotFound
    }
  }

  def add: Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        val id = PrivilegeDao.add(json.as[PrivilegeRow])
        Ok(Json.toJson(PrivilegeDao.get(id)))
      case _ => BadRequest
    }
  }

  def delete(id: String): Action[AnyContent] = isLogged {
    PrivilegeDao.delete(id) match {
      case 1 => Ok
      case _ => NotFound
    }
  }

  def update(id: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        PrivilegeDao.update(id, json.as[PrivilegeRow]) match {
          case Some(result) => Ok(Json.toJson(result))
          case None => NotFound
        }
      case None => BadRequest
    }
  }
}
