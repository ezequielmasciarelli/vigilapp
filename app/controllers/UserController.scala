package controllers

import java.time.Clock

import actions.{GetJWT, HasPermission}
import daos.{OrganizationDao, UserDao}
import database.Tables.UserRow
import javax.inject._
import org.mindrot.jbcrypt.BCrypt
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim}
import play.api.libs.json.{Json, Reads}
import play.api.mvc._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class UserController @Inject()(cc: ControllerComponents,
                               getJWT: GetJWT,
                               hasPermission: HasPermission
                              ) extends AbstractController(cc) {

  private val isLogged = getJWT andThen hasPermission

  case class IdBody(id: String)

  implicit val privilegeRead: Reads[IdBody] = Json.reads[IdBody]
  implicit val clock: Clock = Clock.systemUTC

  def get(id: String): Action[AnyContent] = isLogged {
    UserDao.get(id)
      .map(_.copy(password = null))
      .map(data => Ok(Json.toJson(data)))
      .getOrElse(NotFound)
  }

  def getUserRolesForOrganization(userId: String, orgId: String): Action[AnyContent] = isLogged {
    UserDao.get(userId) match {
      case Some(_) => Ok(Json.toJson(UserDao.getRolesForOrganization(userId, orgId)))
      case _ => NotFound
    }
  }

  def addUserRolesToOrganization(userId: String, orgId: String): Action[AnyContent] = isLogged { implicit request =>
    UserDao.get(userId)
      .flatMap { _ => OrganizationDao.get(orgId) }
      .flatMap { _ => request.body.asJson }
      .map {
        _.as[IdBody].id
      }
      .map {
        UserDao.addUserRolesToOrganization(userId, orgId, _)
      }
      .map { affectedRows => if (affectedRows == 1) Ok else BadRequest }
      .getOrElse(BadRequest)
  }

  def removeRoleForUserOrganization(userId: String, orgId: String): Action[AnyContent] = isLogged { implicit request =>
    request.body.asJson match {
      case Some(json) =>
        val roleToRemove = json.as[IdBody].id
        UserDao.removeRoleForUserOrganization(userId, orgId, roleToRemove) match {
          case 1 => Ok
          case _ => NotFound
        }
      case _ => BadRequest
    }
  }

  case class LoginBody(email: String, password: String)

  implicit val loginRead: Reads[LoginBody] = Json.reads[LoginBody]

  def login: Action[AnyContent] = Action { implicit request =>
    request.body.asJson match {
      case Some(body) =>
        val loginData = body.as[LoginBody]
        UserDao.getByEmail(loginData.email) match {
          case Some(userInDatabase) =>
            val isPasswordCorrect = BCrypt.checkpw(loginData.password, userInDatabase.password)
            if (isPasswordCorrect) {
              val userWithoutPassword = userInDatabase.copy(password = "")
              val token = Jwt.encode(JwtClaim(Json.toJson(userWithoutPassword).toString()).issuedNow.expiresIn(3600 * 4), "secretKey", JwtAlgorithm.HS256)
              Ok.withHeaders("Authorization" -> token)
            }
            else Unauthorized
          case _ => NotFound
        }
      case _ => BadRequest
    }
  }

  def add: Action[AnyContent] = Action { implicit request =>
    request.body.asJson
      .map(_.as[UserRow])
      .filterNot(row => UserDao.exist(row.email))
      .map { row => row.copy(password = BCrypt.hashpw(row.password, BCrypt.gensalt())) }
      .map { row => UserDao.add(row) }
      .flatMap { userId => UserDao.get(userId) }
      .map { userRow => userRow.copy(password = null) }
      .map { userRow => Ok(Json.toJson(userRow)) }
      .getOrElse(BadRequest)

  }

  def delete(id: String): Action[AnyContent] = isLogged {
    UserDao.delete(id) match {
      case 1 => Ok
      case _ => NotFound
    }
  }

  def update(id: String): Action[AnyContent] = isLogged {
    implicit request =>
      request.body.asJson match {
        case Some(userJson) =>
          UserDao.update(id, userJson.as[UserRow]) match {
            case Some(user) => Ok(Json.toJson(user))
            case None => NotFound
          }
        case None => BadRequest
      }
  }
}
