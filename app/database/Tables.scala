package database

import play.api.libs.json.{Json, OFormat, OWrites, Reads}
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.MySQLProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile

  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  implicit val writes: OWrites[OrganizationRow] = Json.using[Json.WithDefaultValues].writes[OrganizationRow]
  implicit val read: Reads[OrganizationRow] = Json.using[Json.WithDefaultValues].reads[OrganizationRow]
  implicit val format: OFormat[OrganizationRow] = Json.using[Json.WithDefaultValues].format[OrganizationRow]

  implicit val userWrites: OWrites[UserRow] = Json.using[Json.WithDefaultValues].writes[UserRow]
  implicit val userRead: Reads[UserRow] = Json.using[Json.WithDefaultValues].reads[UserRow]
  implicit val userFormat: OFormat[UserRow] = Json.using[Json.WithDefaultValues].format[UserRow]

  implicit val privilegeWrites: OWrites[PrivilegeRow] = Json.using[Json.WithDefaultValues].writes[PrivilegeRow]
  implicit val privilegeRead: Reads[PrivilegeRow] = Json.using[Json.WithDefaultValues].reads[PrivilegeRow]
  implicit val privilegeFormat: OFormat[PrivilegeRow] = Json.using[Json.WithDefaultValues].format[PrivilegeRow]

  implicit val roleWrites: OWrites[RoleRow] = Json.using[Json.WithDefaultValues].writes[RoleRow]
  implicit val roleRead: Reads[RoleRow] = Json.using[Json.WithDefaultValues].reads[RoleRow]
  implicit val roleFormat: OFormat[RoleRow] = Json.using[Json.WithDefaultValues].format[RoleRow]

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(Organization.schema, Privilege.schema, Role.schema, RolePrivilege.schema, User.schema, UserOrganization.schema, UserOrganizationRole.schema).reduceLeft(_ ++ _)

  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Organization
   *
   * @param id            Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true)
   * @param name          Database column name SqlType(TEXT)
   * @param fiscalAddress Database column fiscal_address SqlType(TEXT) */
  case class OrganizationRow(id: String = "", name: String, fiscalAddress: String)

  /** GetResult implicit for fetching OrganizationRow objects using plain SQL queries */
  implicit def GetResultOrganizationRow(implicit e0: GR[String]): GR[OrganizationRow] = GR {
    prs =>
      import prs._
      OrganizationRow.tupled((<<[String], <<[String], <<[String]))
  }

  /** Table description of table ORGANIZATION. Objects of this class serve as prototypes for rows in queries. */
  class Organization(_tableTag: Tag) extends profile.api.Table[OrganizationRow](_tableTag, Some("vigilapp"), "ORGANIZATION") {
    def * = (id, name, fiscalAddress) <> (OrganizationRow.tupled, OrganizationRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(name), Rep.Some(fiscalAddress))).shaped.<>({ r => import r._; _1.map(_ => OrganizationRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column name SqlType(TEXT) */
    val name: Rep[String] = column[String]("name")
    /** Database column fiscal_address SqlType(TEXT) */
    val fiscalAddress: Rep[String] = column[String]("fiscal_address")
  }

  /** Collection-like TableQuery object for table Organization */
  lazy val Organization = new TableQuery(tag => new Organization(tag))

  /** Entity class storing rows of table Privilege
   *
   * @param id          Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true)
   * @param `type`      Database column type SqlType(TEXT)
   * @param description Database column description SqlType(TEXT) */
  case class PrivilegeRow(id: String = "", `type`: String, description: String)

  /** GetResult implicit for fetching PrivilegeRow objects using plain SQL queries */
  implicit def GetResultPrivilegeRow(implicit e0: GR[String]): GR[PrivilegeRow] = GR {
    prs =>
      import prs._
      PrivilegeRow.tupled((<<[String], <<[String], <<[String]))
  }

  /** Table description of table PRIVILEGE. Objects of this class serve as prototypes for rows in queries.
   * NOTE: The following names collided with Scala keywords and were escaped: type */
  class Privilege(_tableTag: Tag) extends profile.api.Table[PrivilegeRow](_tableTag, Some("vigilapp"), "PRIVILEGE") {
    def * = (id, `type`, description) <> (PrivilegeRow.tupled, PrivilegeRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(`type`), Rep.Some(description))).shaped.<>({ r => import r._; _1.map(_ => PrivilegeRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column type SqlType(TEXT)
     * NOTE: The name was escaped because it collided with a Scala keyword. */
    val `type`: Rep[String] = column[String]("type")
    /** Database column description SqlType(TEXT) */
    val description: Rep[String] = column[String]("description")
  }

  /** Collection-like TableQuery object for table Privilege */
  lazy val Privilege = new TableQuery(tag => new Privilege(tag))

  /** Entity class storing rows of table Role
   *
   * @param id             Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true)
   * @param name           Database column name SqlType(TEXT)
   * @param organizationId Database column organization_id SqlType(VARCHAR), Length(255,true) */
  case class RoleRow(id: String = "", name: String, organizationId: String)

  /** GetResult implicit for fetching RoleRow objects using plain SQL queries */
  implicit def GetResultRoleRow(implicit e0: GR[String]): GR[RoleRow] = GR {
    prs =>
      import prs._
      RoleRow.tupled((<<[String], <<[String], <<[String]))
  }

  /** Table description of table ROLE. Objects of this class serve as prototypes for rows in queries. */
  class Role(_tableTag: Tag) extends profile.api.Table[RoleRow](_tableTag, Some("vigilapp"), "ROLE") {
    def * = (id, name, organizationId) <> (RoleRow.tupled, RoleRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(name), Rep.Some(organizationId))).shaped.<>({ r => import r._; _1.map(_ => RoleRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column name SqlType(TEXT) */
    val name: Rep[String] = column[String]("name")
    /** Database column organization_id SqlType(VARCHAR), Length(255,true) */
    val organizationId: Rep[String] = column[String]("organization_id", O.Length(255, varying = true))

    /** Foreign key referencing Organization (database name ROLE_ORGANIZATION_id_fk) */
    lazy val organizationFk = foreignKey("ROLE_ORGANIZATION_id_fk", organizationId, Organization)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table Role */
  lazy val Role = new TableQuery(tag => new Role(tag))

  /** Entity class storing rows of table RolePrivilege
   *
   * @param rolId       Database column rol_id SqlType(VARCHAR), Length(255,true)
   * @param privilegeId Database column privilege_id SqlType(VARCHAR), Length(255,true) */
  case class RolePrivilegeRow(rolId: String, privilegeId: String)

  /** GetResult implicit for fetching RolePrivilegeRow objects using plain SQL queries */
  implicit def GetResultRolePrivilegeRow(implicit e0: GR[String]): GR[RolePrivilegeRow] = GR {
    prs =>
      import prs._
      RolePrivilegeRow.tupled((<<[String], <<[String]))
  }

  /** Table description of table ROLE_PRIVILEGE. Objects of this class serve as prototypes for rows in queries. */
  class RolePrivilege(_tableTag: Tag) extends profile.api.Table[RolePrivilegeRow](_tableTag, Some("vigilapp"), "ROLE_PRIVILEGE") {
    def * = (rolId, privilegeId) <> (RolePrivilegeRow.tupled, RolePrivilegeRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(rolId), Rep.Some(privilegeId))).shaped.<>({ r => import r._; _1.map(_ => RolePrivilegeRow.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column rol_id SqlType(VARCHAR), Length(255,true) */
    val rolId: Rep[String] = column[String]("rol_id", O.Length(255, varying = true))
    /** Database column privilege_id SqlType(VARCHAR), Length(255,true) */
    val privilegeId: Rep[String] = column[String]("privilege_id", O.Length(255, varying = true))

    /** Primary key of RolePrivilege (database name ROLE_PRIVILEGE_PK) */
    val pk = primaryKey("ROLE_PRIVILEGE_PK", (rolId, privilegeId))

    /** Foreign key referencing Privilege (database name ROLE_PRIVILEGE_PRIVILEGE_id_fk) */
    lazy val privilegeFk = foreignKey("ROLE_PRIVILEGE_PRIVILEGE_id_fk", privilegeId, Privilege)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Role (database name ROLE_PRIVILEGE_ROLE_id_fk) */
    lazy val roleFk = foreignKey("ROLE_PRIVILEGE_ROLE_id_fk", rolId, Role)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table RolePrivilege */
  lazy val RolePrivilege = new TableQuery(tag => new RolePrivilege(tag))

  /** Entity class storing rows of table User
   *
   * @param id        Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true)
   * @param email     Database column email SqlType(VARCHAR), Length(255,true)
   * @param firstName Database column first_name SqlType(TEXT)
   * @param lastName  Database column last_name SqlType(TEXT)
   * @param bornDate  Database column born_date SqlType(DATE)
   * @param gender    Database column gender SqlType(TEXT)
   * @param password  Database column password SqlType(TEXT)
   * @param active    Database column active SqlType(BIT) */
  case class UserRow(id: String = "", email: String, firstName: String, lastName: String, bornDate: java.sql.Date, gender: String, password: String, active: Boolean)

  /** GetResult implicit for fetching UserRow objects using plain SQL queries */
  implicit def GetResultUserRow(implicit e0: GR[String], e1: GR[java.sql.Date], e2: GR[Boolean]): GR[UserRow] = GR {
    prs =>
      import prs._
      UserRow.tupled((<<[String], <<[String], <<[String], <<[String], <<[java.sql.Date], <<[String], <<[String], <<[Boolean]))
  }

  /** Table description of table USER. Objects of this class serve as prototypes for rows in queries. */
  class User(_tableTag: Tag) extends profile.api.Table[UserRow](_tableTag, Some("vigilapp"), "USER") {
    def * = (id, email, firstName, lastName, bornDate, gender, password, active) <> (UserRow.tupled, UserRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(email), Rep.Some(firstName), Rep.Some(lastName), Rep.Some(bornDate), Rep.Some(gender), Rep.Some(password), Rep.Some(active))).shaped.<>({ r => import r._; _1.map(_ => UserRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val id: Rep[String] = column[String]("id", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column email SqlType(VARCHAR), Length(255,true) */
    val email: Rep[String] = column[String]("email", O.Length(255, varying = true))
    /** Database column first_name SqlType(TEXT) */
    val firstName: Rep[String] = column[String]("first_name")
    /** Database column last_name SqlType(TEXT) */
    val lastName: Rep[String] = column[String]("last_name")
    /** Database column born_date SqlType(DATE) */
    val bornDate: Rep[java.sql.Date] = column[java.sql.Date]("born_date")
    /** Database column gender SqlType(TEXT) */
    val gender: Rep[String] = column[String]("gender")
    /** Database column password SqlType(TEXT) */
    val password: Rep[String] = column[String]("password")
    /** Database column active SqlType(BIT) */
    val active: Rep[Boolean] = column[Boolean]("active")

    /** Uniqueness Index over (email) (database name USER_email_uindex2) */
    val index1 = index("USER_email_uindex2", email, unique = true)
  }

  /** Collection-like TableQuery object for table User */
  lazy val User = new TableQuery(tag => new User(tag))

  /** Entity class storing rows of table UserOrganization
   *
   * @param organizationId Database column organization_id SqlType(VARCHAR), Length(255,true)
   * @param userId         Database column user_id SqlType(VARCHAR), Length(255,true) */
  case class UserOrganizationRow(organizationId: String, userId: String)

  /** GetResult implicit for fetching UserOrganizationRow objects using plain SQL queries */
  implicit def GetResultUserOrganizationRow(implicit e0: GR[String]): GR[UserOrganizationRow] = GR {
    prs =>
      import prs._
      UserOrganizationRow.tupled((<<[String], <<[String]))
  }

  /** Table description of table USER_ORGANIZATION. Objects of this class serve as prototypes for rows in queries. */
  class UserOrganization(_tableTag: Tag) extends profile.api.Table[UserOrganizationRow](_tableTag, Some("vigilapp"), "USER_ORGANIZATION") {
    def * = (organizationId, userId) <> (UserOrganizationRow.tupled, UserOrganizationRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(organizationId), Rep.Some(userId))).shaped.<>({ r => import r._; _1.map(_ => UserOrganizationRow.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column organization_id SqlType(VARCHAR), Length(255,true) */
    val organizationId: Rep[String] = column[String]("organization_id", O.Length(255, varying = true))
    /** Database column user_id SqlType(VARCHAR), Length(255,true) */
    val userId: Rep[String] = column[String]("user_id", O.Length(255, varying = true))

    /** Primary key of UserOrganization (database name USER_ORGANIZATION_PK) */
    val pk = primaryKey("USER_ORGANIZATION_PK", (organizationId, userId))

    /** Foreign key referencing Organization (database name USER_ORGANIZATION_ORGANIZATION_id_fk) */
    lazy val organizationFk = foreignKey("USER_ORGANIZATION_ORGANIZATION_id_fk", organizationId, Organization)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing User (database name USER_ORGANIZATION_USER_id_fk) */
    lazy val userFk = foreignKey("USER_ORGANIZATION_USER_id_fk", userId, User)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table UserOrganization */
  lazy val UserOrganization = new TableQuery(tag => new UserOrganization(tag))

  /** Entity class storing rows of table UserOrganizationRole
   *
   * @param userId         Database column user_id SqlType(VARCHAR), Length(255,true)
   * @param organizationId Database column organization_id SqlType(VARCHAR), Length(255,true)
   * @param roleId         Database column role_id SqlType(VARCHAR), Length(255,true) */
  case class UserOrganizationRoleRow(userId: String, organizationId: String, roleId: String)

  /** GetResult implicit for fetching UserOrganizationRoleRow objects using plain SQL queries */
  implicit def GetResultUserOrganizationRoleRow(implicit e0: GR[String]): GR[UserOrganizationRoleRow] = GR {
    prs =>
      import prs._
      UserOrganizationRoleRow.tupled((<<[String], <<[String], <<[String]))
  }

  /** Table description of table USER_ORGANIZATION_ROLE. Objects of this class serve as prototypes for rows in queries. */
  class UserOrganizationRole(_tableTag: Tag) extends profile.api.Table[UserOrganizationRoleRow](_tableTag, Some("vigilapp"), "USER_ORGANIZATION_ROLE") {
    def * = (userId, organizationId, roleId) <> (UserOrganizationRoleRow.tupled, UserOrganizationRoleRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(userId), Rep.Some(organizationId), Rep.Some(roleId))).shaped.<>({ r => import r._; _1.map(_ => UserOrganizationRoleRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(VARCHAR), Length(255,true) */
    val userId: Rep[String] = column[String]("user_id", O.Length(255, varying = true))
    /** Database column organization_id SqlType(VARCHAR), Length(255,true) */
    val organizationId: Rep[String] = column[String]("organization_id", O.Length(255, varying = true))
    /** Database column role_id SqlType(VARCHAR), Length(255,true) */
    val roleId: Rep[String] = column[String]("role_id", O.Length(255, varying = true))

    /** Primary key of UserOrganizationRole (database name USER_ORGANIZATION_ROLE_PK) */
    val pk = primaryKey("USER_ORGANIZATION_ROLE_PK", (userId, organizationId, roleId))

    /** Foreign key referencing Organization (database name USER_ORGANIZATION_ROLE_ORGANIZATION_id_fk) */
    lazy val organizationFk = foreignKey("USER_ORGANIZATION_ROLE_ORGANIZATION_id_fk", organizationId, Organization)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Role (database name USER_ORGANIZATION_ROLE_ROLE_id_fk) */
    lazy val roleFk = foreignKey("USER_ORGANIZATION_ROLE_ROLE_id_fk", roleId, Role)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing User (database name USER_ORGANIZATION_ROLE_USER_id_fk) */
    lazy val userFk = foreignKey("USER_ORGANIZATION_ROLE_USER_id_fk", userId, User)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table UserOrganizationRole */
  lazy val UserOrganizationRole = new TableQuery(tag => new UserOrganizationRole(tag))
}
