name := "gaboplay"

version := "1.0"

lazy val `gaboplay` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"

scalaVersion := "2.12.10"

libraryDependencies ++= Seq(jdbc, ehcache, ws, specs2 % Test, guice)

//LAS MIAS DE SLICK
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.3.2"
libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.3.2"
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.15"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
//FIN MIAS

//Hash password
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"
libraryDependencies ++= Seq(
  "com.pauldijou" %% "jwt-core" % "4.2.0"
)

// https://mvnrepository.com/artifact/org.apache.commons/commons-email
libraryDependencies += "org.apache.commons" % "commons-email" % "1.5"

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")